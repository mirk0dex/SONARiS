    Copyright (C) 2021-2022 mirk0dex
    <mirk0dex.gitlab.io>
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# SONARiS

SONARiS is a script that lets you consoom SoundCloud from the terminal. Its name
was chosen rather randomly, so just ignore how it sounds like *Solaris™*, it's
**100%** not wanted please Oracle™ do not sue me thanks.

It, by default, makes use of the amazing
[MPV](https://github.com/mpv-player/mpv) program as well as the portentous
[youtube-dl](https://github.com/ytdl-org/youtube-dl) to deliver you a
high-quality SoundCloud listening experience directly from your terminal/CLI.

The script is meant to never exceed *200 SLOC*.

## Installation

To install SONARiS, simply don't install it, lol. Just ``git clone
https://gitlab.com/mirk0dex/sonaris`` and ``cd`` into the new folder, which will
probably be called "sonaris" or something like that. Now you can run the script
by doing ``./sonaris.sh``, but please make sure that:
1. you have installed all deps listed below,
2. you have run ``chmod +x sonaris.sh`` to ensure that the script is executable.

## Dependencies

The script needs the following (amazing, I shall add) programs to be installed
to work properly. They are:
- **git**, to clone the repo;
- a **UNIX-like system** with a **home folder**; basically means that there is
  **no Boomer OS (Windows) support**, install GNU/Linux folks (still, you might
  be able to get it running inside WSL if you really wanted to);
- **bash** (bloat alert);
- **GNU sed**;
- **grep** (replaceable with **ripgrep**);
- a command line finder, default is **fzf**;
- **curl**;
- **youtube-dl** (I'd just like to interject for a moment: what you're referring
  to as 'youtube-dl', is in fact 'youtube/soundcloud-dl', or, as I've recently
  taken to calling it, 'youtube + soundcloud-dl'.);
- a media/music/audio player that supports youtube-dl (of course), default is
  **mpv**.

To quickly install alle these, you can use one of the following commands, feel
free to pick the one that suits your distro (or OS) the better.

*Ubuntu*/*Debian*/*Elementary OS*/*Linux Mint*/Any Ubuntu or Debian-based
distro: ``su -c "apt-get update -y && apt-get upgrade && apt-get install -y git
bash sed grep fzf curl mpv youtube-dl"``

*Arch Linux*/*Manjaro*/*Artix Linux*/*Endeavour OS*/*Parabola GNU/Linux*/Any
Arch-based distro: ``su -c "pacman -Syu && pacman -S --noconfirm git bash sed
grep fzf curl mpv youtube-dl"``

*Void GNU/Linux*: ``su -c "xbps-install -Su && xbps-install git bash sed grep fzf curl mpv youtube-dl"``

*FreeBSD*: ``su -c "pkg install git bash gsed fzf curl mpv youtube_dl"``

*OpenBSD*: ``doas pkg_add git bash gsed fzf curl mpv youtube-dl"``

### Proprietary systems

**Ouch, looks like you accidentally wasted $10,000 on that non-free piece of
poop!**

Don't worry, we got you covered too, although it is highly recommended to never
use proprietary software. Remember to digit [gnu.org](https://gnu.org) in the
address bar rather than
[appel.com](https://img.memecdn.com/wow-such-apple_o_2675543.jpg), next time.

*MacOS* with *brew*: ``brew install git bash gnu-sed fzf curl mpv youtube-dl``

## Usage

See './sonaris.sh --help' for info on SONARiS and how to use it.

Generally speaking, you just have to run './sonaris.sh' and enter what you want
it to look for on SoundCloud, then pick one of the results.

## Contributing

See '[CONTRIBUTING.md](CONTRIBUTING.md)'.

## Hacking & configuring

Just open up the script in your favoUrite text editor and start hacking on it.
To change options such as the default player, you just have to change a
variable. Since this is free software, feel free to fork this repo and do
whatever you want to this script (except, of course, killing it: don't you dare
killing my creation!).

## Known bugs

The program's song downloading feature is still buggy and using it is not
recommended (yet).

## To-Do

- [x] Make the program support downloading songs from SoundCloud
- [ ] Make the program REALLY support downloading songs from SoundCloud
- [x] Enable "favourite artists" support
